#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#define ROWM 0
#define COLM 1

void print_range_1d(const int * vector, const int start, const int end, const int count);
void print_1d(const int * vector, const int size);
void print_2d(const int * matrix, const int row_len, const int col_len, const int type);
void print_details(const int type, const int dim, const int * shape);
int * to_row_major(int ** matrix, const int row_len, const int col_len);
void print_pointers(int * structure, int size);

void print_array(const int * data, const int dim, const int type, ...)
{
    va_list valist;
    int shape[dim];
    int i, j, k, c = 0;
    int total = 1;

    va_start(valist, type);
    for(i = 0; i < dim; i++)
    {
        shape[i] = va_arg(valist, int);
        total *= shape[i];
    }
    va_end(valist);

    print_details(type, dim, shape);
    if(dim > 2)
    {
        // Does this need to be implemented?
    } else if(dim > 1) {
        print_2d(data, shape[0], shape[1], type);
    } else {
        print_1d(data, shape[0]);
    }
    printf("\n");
}

void print_range_1d(const int * vector, const int start, const int end, const int count)
{
    int i, c;
    for(i = start, c = start; i < end; i += count)
    {
        printf("%d", vector[i]);
        c += count;
        if(c < end)
        {
            printf(", ");
        }
    }
}

void print_1d(const int * vector, const int size)
{
    printf("[");
    print_range_1d(vector, 0, size, 1);
    printf("]\n");
}

void print_2d(const int * matrix, const int row_len, const int col_len, const int type)
{
    int i, j, c = 0;
    int * x;

    if(type == COLM)
    {
        x = to_row_major((int **) &matrix, row_len, col_len);
    } else {
        x = (int *) matrix;
    }

    printf("[\n");
    for(i = 0; i < row_len; i++)
    {
        printf(" [");
        print_range_1d(x, i * col_len, (i * col_len) + col_len, 1);
        printf("]\n");
    }
    printf("]\n");
}

void print_details(const int type, const int dim, const int * shape)
{
    printf("ITYPE: %s\n", type == ROWM ? "Row-Major" : "Column-Major - converted to Row-Major");
    printf("  DIM: %d\n", dim);
    printf("SHAPE: ");
    print_1d(shape, dim);
}

int * to_row_major(int ** matrix, const int col_len, const int row_len)
{
    int i, j, index = 0;
    int * g = (int *) malloc(sizeof(int) * col_len * row_len);

    for(i = 0; i < col_len; i++)
    {
        for(j = i; j < row_len * col_len; j += col_len)
        {
            g[index++] = *(*matrix + j);
        }
    }

    
//    print_array(matrix, 1, 0, col_len * row_len);
//    print_array(*g, 1, 0, col_len * row_len);
//    for(j = 0; j < row_len * col_len; j ++)
//    {
//        printf("%d ", *(matrix + j));
//    }
//    printf("\n");
//    for(j = 0; j < row_len * col_len; j ++)
//    {
//        printf("%d ", **(g + j));
//    }
//    printf("\n");
//
    for(i = 0; i < col_len * row_len; i++)
    {
        printf("index %2d : value %3d : %p\n", i, *(*matrix + i), (void *) (*matrix + i));
    }
    print_pointers(g, col_len * row_len);
//
//    print_array(*g, 2, 0, col_len, row_len);

    //free(*matrix);

    return g;
}

void print_pointers(int * structure, int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        printf("index %2d : value %3d : %p\n", i, *(structure + i), (void *) (structure + i));
    }
}
