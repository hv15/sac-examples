// FortranArrays.c - C interface to test out the FortranArrays C-library

// Hans-Nikolai Viessmann 04/07/2014 [Initial Version]

#include <stdio.h>
#include <stdlib.h>

#include "cwrapper.h"

int main(int argc, char **argv)
{
    int i;
    int square = 11;
    int *array;
    int *result, *props;
    SACarg *SACsquare, *SACarray, *SACresult, *SACprops;
    //SAChive *hive;

    //SAC_InitRuntimeSystem();
    
    array = (int *) malloc(sizeof(int) * square * square);
    for(i = 0; i < square*square; i++)
    {
        *(array + i) = i + 1;
    }

    //SACsquare = SACARGconvertFromIntScalar(square);
    SACsquare = SACARGconvertFromIntPointer(array, 2, square, square);
    
    //hive = SAC_AllocHive(1, 0, NULL, NULL);
    //SAC_AttachHive(hive);

    //FortranArrays__create_matrix1(&SACarray, SACsquare);
    //FortranArrays__matrix_properties1(&SACprops, SACsquare);
    FortranArrays__sum_columns1(&SACresult, SACsquare);
    
    //SAC_ReleaseQueen();
    //hive = SAC_DetachHive();

    //props = SACARGconvertToIntArray(SACprops);
    result = SACARGconvertToIntArray(SACresult);

    //for(i = 0; i < 7; i++)
    //{
    //    printf("%d\n", *(props + i));
    //}

    for(i = 0; i < square; i++)
    {
        printf("%d\n", *(result + i));
    }
    //SAC_FreeRuntimeSystem();
    return 0;
} 
