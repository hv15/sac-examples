// TotientRance.c - Sequential Euler Totient Function (C Version)
// _compile: gcc -Wall -O -o TotientRange TotientRange.c_
// compile: gcc -Wall -pedantic -std=gnu99 -o TotientRange TotientRange.c
// run:     ./TotientRange lower_num uppper_num

// Greg Michaelson        14/10/2003
// Patrick Maier          29/01/2010 [enforced ANSI C compliance]
// Hans-Nikolai Viessmann 16/02/2014 [For CW1 F21DP, output time]
// Hans-Nikolai Viessmann 12/06/2014 [Altered for use with SAC4C]

// This program calculates the sum of the totients between a lower and an 
// upper limit using C longs. It is based on earlier work by:
// Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "cwrapper.h"

static int const NUM_THREADS = 4;

// Some time related functions, in order to get meaningful runtime

double elapsed(struct timespec a, struct timespec b)
{
    return ((double)(b.tv_sec - a.tv_sec)) + 
        ((double)(b.tv_nsec - a.tv_nsec) / 1000000000.0);
}

int main(int argc, char ** argv)
{
    struct timespec start, stop;
    int lower, upper;
    double *result;
    SACarg *SAClower, *SACupper, *SACresult;
    SAChive *hive;

    if (argc != 3) {
        fprintf(stderr, "Usage: LOWER UPPER [-mt NUM_THREADS]\n"
                "If NUM_THREADS not set, defaults to 4 threads.\n");
        return -1;
    }
    sscanf(argv[1], "%d", &lower);
    sscanf(argv[2], "%d", &upper);

    SAC_InitRuntimeSystem();

    SAClower = SACARGconvertFromIntScalar(lower);
    SACupper = SACARGconvertFromIntScalar(upper);
    
    hive = SAC_AllocHive(NUM_THREADS, 0, NULL, NULL);
    printf("Sizeof(hive): %d\n", sizeof(hive));
    SAC_AttachHive(hive);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);

    //result = sumTotient(lower, upper);
    TotientRange__sumTotient2( &SACresult, SAClower, SACupper);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    
    SAC_ReleaseQueen();
    hive = SAC_DetachHive();

    result = SACARGconvertToDoubleArray(SACresult);

    printf("%f %d %d %f\n", elapsed(start, stop), lower, upper, *result);

    //free(hive);
    SAC_FreeRuntimeSystem();
    return EXIT_SUCCESS;
}
