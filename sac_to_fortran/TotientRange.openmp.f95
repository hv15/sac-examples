! TotientRange.f90 - Sequential and Parallel Euler Totient Function (Fortran)
! compile: gfortran -fopenmp -o TotientRange TotientRange.f95
!     run: ./TotientRange lower_num upper_num
! set thread number, export the OMP_NUM_THREADS variable to the shell
! environment

! Based upon the C version created by Greg Michaelson
! Hans-Nikolai Viessmann 14/06/2014 [Converted to FORTRAN 90/95]
! Hans-Nikolai Viessmann 15/06/2014 [Make parallel using OpenMP runtime]

! This program calculates the sum of the totients between a lower and an 
! upper limit using Fortran INTEGERs. It is based on earlier work by:
! Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

      PROGRAM TotientRange
          !$ USE omp_lib ! Provides OMP functions
          IMPLICIT NONE ! Prevent default variables from being created
          INTEGER                 :: lower_num, upper_num ! inputs
          INTEGER                 :: s                    ! summation
          INTEGER                 :: start, finish        ! time related
          ! Argument Parsing
          INTEGER                 :: arg_count   ! arg count
          CHARACTER(LEN = 32)     :: c           ! temp variable
          INTEGER                 :: thread_num  ! number of threads

          arg_count = COMMAND_ARGUMENT_COUNT()
          IF (arg_count /= 2) THEN
              PRINT *, "Wrong arguments: lower upper"
              CALL EXIT(-1)
          END IF

          CALL GET_COMMAND_ARGUMENT(1, c)
          READ (c, *) lower_num

          CALL GET_COMMAND_ARGUMENT(2, c)
          READ (c, *) upper_num 

          CALL SYSTEM_CLOCK(start)

          s = sumTotient(lower_num, upper_num)

          CALL SYSTEM_CLOCK(finish)

          WRITE (*, "(*(I0,X))") finish - start, lower_num, upper_num, &
                                 s, thread_num

      CONTAINS
    
          INTEGER FUNCTION hcf(x, y)
              INTEGER, INTENT(IN)     :: x, y    ! inputs
              INTEGER                 :: z, w, v ! intermediary value
    
              w = x
              v = y
              DO WHILE (v /= 0)
                  z = MOD(w, v)
                  w = v
                  v = z
              END DO
              hcf = w
          END FUNCTION hcf
    
          LOGICAL FUNCTION relprime(x, y)
              INTEGER, INTENT(IN)     :: x, y ! input

              relprime = hcf(x, y) == 1
          END FUNCTION relprime
    
          INTEGER FUNCTION euler(n)
              INTEGER, INTENT(IN)     :: n      ! input
              INTEGER                 :: length ! intermediary value
              INTEGER                 :: j      ! loop-counter
    
              length = 0
              DO j = 1, n - 1
                  IF (relprime(n, j)) THEN
                      length = length + 1
                  END IF
              END DO
              euler = length
          END FUNCTION euler
    
          INTEGER FUNCTION sumTotient(lower, upper)
              INTEGER, INTENT(IN)     :: lower, upper ! inputs
              INTEGER                 :: i            ! loop-counter
              INTEGER                 :: s            ! summation
    
              s = 0
              !$OMP PARALLEL
              !$ thread_num = OMP_get_num_threads()
              !$OMP DO SCHEDULE(GUIDED) PRIVATE(i) REDUCTION(+:s)
              DO i = lower, upper
                  s = s + euler(i)
              END DO
              !$OMP END DO
              !$OMP END PARALLEL
              sumTotient = s
          END FUNCTION sumTotient

      END PROGRAM TotientRange